package situacion_3;
public class Principal {
    

    public static void main(String[] args) {
        
        Propietario Cliente1 = new Propietario();
        Automovil auto1 = new Automovil();
        Reparaciones autoreparado = new Reparaciones();

        Cliente1.setDNI(32564851);
        Cliente1.setNombre("Facundo");
        Cliente1.setApellido("Monasterio");
        Cliente1.setDireccion("Av, Circunvalacion 564");
        Cliente1.setTelefono(381569824);
 
        auto1.setMarca("Toyota");
        auto1.setModelo("Etios");
        auto1.setMatricula("FJG 854");
        auto1.setKilometraje(60000);
        auto1.setMotivo("Chapa, pintura y cambio de faro, parte trasera");

        autoreparado.setReparaciones("Arreglo de choque, y pintura nueva");
        autoreparado.setRepuestos("Pintura, faro trasero izquierdo");
        autoreparado.setManoDeObra(80.500);


        System.out.println(" " + Cliente1.toString());
        System.out.println(" " + auto1.toString());
        System.out.println(" " + autoreparado.toString());

    }


}
