package situacion_3;

public class Reparaciones {
    
    private String reparaciones;
    private String repuestos;
    private double manoDeObra;

    public void setReparaciones(String reparaciones){
        this.reparaciones = reparaciones;
    }

    public String getReparaciones(){
        return reparaciones;
    }

    public void setRepuestos(String repuestos){
        this.repuestos = repuestos;
    }

    public String getRepuestos(){
        return repuestos;
    }

    public void setManoDeObra(double manoDeObra){
        this.manoDeObra = manoDeObra;
    }

    public double getManoDeObra(){
        return manoDeObra;
    }

    public String toString(){
        return "Reparaciones realizadas: " + reparaciones +
        ", Repuestos usados: " + repuestos + 
        ", Costo de mano de obra: " + manoDeObra;
    }
}
