package situacion_3;

public class Automovil {

    private String marca;
    private String modelo;
    private String matricula;
    private int kilometraje;
    private String motivo;


    public void setMarca(String marca){
        this.marca = marca;
    }

    public String getMarca(){
        return marca;
    }

    public void setModelo(String modelo){
        this.modelo = modelo;
    }

    public String getModelo(){
        return modelo;
    }

    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public String getMatricula(){
        return matricula;
    }
    
    public void setKilometraje(int kilometraje){
        this.kilometraje = kilometraje;
    }
    public int getKilometraje(){
        return kilometraje;
    }

    public void setMotivo(String motivo){
        this.motivo = motivo;
    }

    public String getMotivo(){
        return motivo;
    }

    public String toString(){
        return "Marca: " + marca +
        ", Modelo: " + modelo + 
        ", Matricula: " + matricula +
        ", Kilometros: " + kilometraje + 
        ", Motivo: " + motivo;
    }
}
