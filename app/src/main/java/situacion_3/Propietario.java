package situacion_3;

public class Propietario {

    private int DNI;
    private String nombre;
    private String apellido;
    private String direccion;
    private int telefono;

    
    public void setDNI(int DNI){
        this.DNI = DNI;
    }

    public int getDNi(){
        return DNI;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public String getApellido(){
        return apellido;
    }

    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public String getDireccion(){
        return direccion;
    }

    public void setTelefono(int telefono){
        this.telefono = telefono;
    }

    public int getTelefono(){
        return telefono;
    }
    

    public String toString(){
        return "Nombre y Apellido: " + nombre + " " + apellido +
        ", DNI: " + DNI + 
        ", Direccion: " + direccion +
        ", Telefono: " + telefono;
    }
}
