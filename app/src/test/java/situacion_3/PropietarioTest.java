package situacion_3;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PropietarioTest {

    @Test public void setNombrePropietarioTest() {
        Propietario Cliente1 = new Propietario();
        Cliente1.setNombre("Facundo");

        String nombreActual = Cliente1.getNombre();

        assertEquals("Facundo", nombreActual);
    }
    
}
